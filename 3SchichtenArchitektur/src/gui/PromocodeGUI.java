package gui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import logic.IPromoCodeLogic;
import java.awt.Color;

import java.util.Random;

import javax.swing.ImageIcon;

public class PromocodeGUI extends JFrame {

	private static final long serialVersionUID = 2303911575322895620L;

	/**
	 * Create the frame.
	 */
	public PromocodeGUI(IPromoCodeLogic logic) {
		setTitle("PromotionCode Generator 2.0");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 205);

		JPanel panelBackground = new JPanel();
		panelBackground.setBackground(Color.BLACK);
		getContentPane().add(panelBackground, BorderLayout.CENTER);
		panelBackground.setLayout(new BorderLayout(0, 0));

		JLabel lblBeschriftungpromocode = new JLabel("Promocode f\u00FCr doppelte IT$");
		lblBeschriftungpromocode.setForeground(Color.WHITE);
		lblBeschriftungpromocode.setFont(new Font("Arial", Font.BOLD, 12));
		panelBackground.add(lblBeschriftungpromocode, BorderLayout.NORTH);

		JLabel lblPromocode = new JLabel("");
		lblPromocode.setForeground(Color.WHITE);
		lblPromocode.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 18));
		lblPromocode.setHorizontalAlignment(SwingConstants.CENTER);
		panelBackground.add(lblPromocode, BorderLayout.CENTER);

		JLabel lblKappaone = new JLabel("");
		panelBackground.add(lblKappaone, BorderLayout.WEST);

		JLabel lblKappatwo = new JLabel("");
		panelBackground.add(lblKappatwo, BorderLayout.EAST);

		JButton btnNewPromoCode = new JButton("Generiere neuen Promo-Code");
		btnNewPromoCode.setFont(new Font("Arial", Font.BOLD, 12));
		btnNewPromoCode.setBackground(Color.BLACK);
		btnNewPromoCode.setForeground(Color.WHITE);
		panelBackground.add(btnNewPromoCode, BorderLayout.SOUTH);
		
		btnNewPromoCode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Random rand = new Random();
				int i=rand.nextInt();
				if (i==666) { //Seltenes Extra mit Chance 3 zu 2.1 Milliarde
					lblKappaone.setIcon(new ImageIcon(PromocodeGUI.class.getResource("/gui/Kappa.png")));
					lblKappatwo.setIcon(new ImageIcon(PromocodeGUI.class.getResource("/gui/Kappa.png")));
					lblPromocode.setText("GET TROLLED");
				} else {
					if(i==69) {
						lblKappaone.setIcon(new ImageIcon(PromocodeGUI.class.getResource("/gui/Kappa.png")));
						lblKappatwo.setIcon(new ImageIcon(PromocodeGUI.class.getResource("/gui/Kappa.png")));
						lblPromocode.setText("GET TROLLED");
					}else{
						if(i==1337){
							lblKappaone.setIcon(new ImageIcon(PromocodeGUI.class.getResource("/gui/Kappa.png")));
							lblKappatwo.setIcon(new ImageIcon(PromocodeGUI.class.getResource("/gui/Kappa.png")));
							lblPromocode.setText("GET TROLLED");
						}else{
							if(i==420){ //Selteneres Extra mit Chance 1 aus 2.1 Milliarde
								lblKappaone.setIcon(new ImageIcon(PromocodeGUI.class.getResource("/gui/leaf.png")));
								lblKappatwo.setIcon(new ImageIcon(PromocodeGUI.class.getResource("/gui/leaf.png")));
								lblPromocode.setText("420 Blaze It");
								panelBackground.setBackground(Color.GREEN);
								lblBeschriftungpromocode.setForeground(Color.BLACK);
							}else{
								String code = logic.getNewPromoCode();
								lblPromocode.setText(code);
							}
						}
					}
				}
			}
		});
		this.setVisible(true);
	}
}
