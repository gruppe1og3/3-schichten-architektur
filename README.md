Google Docs Dokument: https://docs.google.com/document/d/1NFIpHhO4FJTAqoLnGX32vRzIReXdpA8QEO9fgWIMjoU/edit?usp=sharing

Trello (Scrum):
https://trello.com/b/7HzQ2fB8

Aufgabe:
Diese Software erstellt einen 20stelligen Promotioncode für den IT-Unterricht. Der Auftraggeber wünscht mehrere Veränderungen.

1.Die alte Oberfläche ist langweilig. Erstellen Sie eine neue. (Schicht 1: Präsentationsschicht)

2.Der Promocode soll ab sofort nur noch aus Zahlen bestehen, dafür aber 25 Stellen lang sein. (Schicht 2: Anwendungsschicht)

3.Die Promotioncodes sollen in einer Textdatei gespeichert werden. (Schicht 3: Datenerhaltungsschicht)